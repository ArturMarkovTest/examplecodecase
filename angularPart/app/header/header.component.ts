import {Component, OnInit} from '@angular/core';
import {AuthService} from "../auth-module/auth.service";
import {User} from "../auth-module/abstract/User";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    user: User;

    constructor(private authService: AuthService) {
        this.user = new User();
    }

    ngOnInit() {
        this.authService.eventForHeaderComponent.subscribe(
            () => {
                // console.log(userEmail);
                this.getUser();
            }
        );
    }

    isAuth() {
        return this.authService.isAuthenticated();
    }

    logOut() {
        this.authService.logOut();
    }

    getUser() {
        this.authService.getUser()
            .subscribe(
                response => {
                    console.log(response);
                    this.user.id =  response.json()[0].userId;
                    this.user.email = response.json()[0].email;
                    this.user.role = response.json()[0].role;
                },
                error => console.log(error)
            );
    }

}
