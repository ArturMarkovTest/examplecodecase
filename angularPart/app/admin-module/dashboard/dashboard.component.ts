import {Component, OnInit} from '@angular/core';
import {AdminService} from "../admin.service";
import {User} from "../../auth-module/abstract/User";
declare var swal: any;
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    constructor(private adminService: AdminService) {
    }
    users:Array<User>;

    ngOnInit() {
        this.adminService.getUsers().subscribe(
            users => {
                this.users = users;
            }
        );
    }
    deleteUser(id:string){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(()=> {
            this.adminService.deleteUser(id).subscribe(
                userId=>{
                    let users = this.users;
                    users.forEach((user,index)=> {
                        if(user.id == userId){
                            users.splice(index,1);
                        }
                    });
                }
            );
            swal(
                'Deleted!',
                'User has been deleted.',
                'success'
            );
        });
    }

}
