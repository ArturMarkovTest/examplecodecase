import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "app/admin-module/dashboard/dashboard.component";
import {AdminGuard} from "./admin.guard";


const ADMIN_ROUTES: Routes = [
    {path: 'dashboard', component: DashboardComponent,canActivate:[AdminGuard]},
];

export const adminRouting = RouterModule.forChild(ADMIN_ROUTES);