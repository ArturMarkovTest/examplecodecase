import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import  'rxjs/Rx';
import {AuthService} from "../auth-module/auth.service";

@Injectable()
export class AdminService {

    constructor(private http: Http, private authService: AuthService) {
    }

    getUsers() {
        let token = this.authService.getToken();
        // Authorization  Bearer token
        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });
        return this.http.get('http://localhost:20377/api/admin/getUsers',options)
            .map(
                (response: Response) => {
                    // console.log(response.json());
                    return response.json();
                }
            );
    }

    deleteUser(id:string) {
        const token = this.authService.getToken();
        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });
        return this.http.delete('http://localhost:20377/api/admin/user/'+id ,options).map(
            (response: Response) => {
                return response.json();
            }
        );


    }

}
