import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import {adminRouting} from "./admin.routing";
import {AdminGuard} from "./admin.guard";
import {AdminService} from "./admin.service";

@NgModule({
  imports: [
    CommonModule,
    adminRouting,
  ],
  providers:[AdminGuard,AdminService],
  declarations: [DashboardComponent]
})
export class AdminModuleModule { }
