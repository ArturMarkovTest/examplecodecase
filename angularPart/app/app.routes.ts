import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./home/home.component";

export const APP_ROUTES: Routes = [

    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'auth', loadChildren: 'app/auth-module/auth-module.module#AuthModuleModule'},
    {path: 'admin', loadChildren: 'app/admin-module/admin-module.module#AdminModuleModule'},
    {path: 'posts', loadChildren: 'app/posts-module/posts-module.module#PostsModuleModule'},

];
export const routing = RouterModule.forRoot(APP_ROUTES);