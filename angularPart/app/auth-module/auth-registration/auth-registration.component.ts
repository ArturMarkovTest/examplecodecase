import {Component} from '@angular/core';
import {AuthService} from "../auth.service";
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from "@angular/router";
declare var swal:any;

@Component({
    selector: 'app-auth-registration',
    templateUrl: './auth-registration.component.html',
    styleUrls: ['./auth-registration.component.css']
})
export class AuthRegistrationComponent {
    registrationForm: FormGroup;
    messageSuccess: string = '';
    messageError: string = '';

    constructor(private authService: AuthService,private router: Router) {
        this.registrationForm = new FormGroup({
            'email': new FormControl(null, [<any>Validators.required, <any>Validators.email]),
            'password': new FormControl(null, [<any>Validators.required, <any>Validators.minLength(4)]),
        });
    }

    onSubmit(value: any) {
        this.messageError = '';
        this.messageSuccess = '';
        this.authService.signUp( value.email, value.password)
            .subscribe(
                responce => {
                    console.log(responce);
                    this.registrationForm.reset();
                    swal(
                        'Good job!',
                        'Your account successfully created!',
                        'success'
                    )
                    this.router.navigate(['/login']);
                },
                error => {
                    this.messageError = error._body;
                }
            )
    }

}
