import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-protected-example',
  templateUrl: './protected-example.component.html',
  styleUrls: ['./protected-example.component.css']
})
export class ProtectedExampleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
