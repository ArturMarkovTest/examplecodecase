import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectedExampleComponent } from './protected-example.component';

describe('ProtectedExampleComponent', () => {
  let component: ProtectedExampleComponent;
  let fixture: ComponentFixture<ProtectedExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectedExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectedExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
