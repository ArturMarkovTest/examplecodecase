import {Component} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {AuthService} from "../auth.service";
declare var swal: any;

@Component({
    selector: 'app-auth-login',
    templateUrl: './auth-login.component.html',
    styleUrls: ['./auth-login.component.css']
})
export class AuthLoginComponent {
    loginForm: FormGroup;
    messageError: string = '';

    constructor(private authServise: AuthService) {
        this.loginForm = new FormGroup({
            'email': new FormControl(null, [<any>Validators.required, <any>Validators.email]),
            'password': new FormControl(null, [<any>Validators.required, <any>Validators.minLength(4)]),
        });
    }

    onSubmit(value: any) {
        this.authServise.signIn(value.email, value.password)
            .subscribe(
                () => console.log('Successfully auth'),
                error => {
                    this.messageError = '';
                    this.messageError = error._body;
                }
            );
    }
}
