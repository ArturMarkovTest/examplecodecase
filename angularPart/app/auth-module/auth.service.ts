import {EventEmitter, Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from "@angular/http";
import {Observable} from 'rxjs';
import  'rxjs/Rx';
import {Router} from "@angular/router";
import {User} from "./abstract/User";

@Injectable()
export class AuthService {

    currentUser: User;
    eventForHeaderComponent = new EventEmitter<string>();

    constructor(private http: Http, private router: Router) {
        this.currentUser = new User();
    }

    signUp( email: string, password: string) {
        return this.http.post('http://localhost:20377/api/users/registration', { email: email, password: password},
            {headers: new Headers({'Content-Type': 'application/json'})});
    }

    signIn(email: string, password: string) {
        return this.http.post('http://localhost:20377/api/users/login', {email: email, password: password},
            {headers: new Headers({'Content-Type': 'application/json'})})
            .map(
                (respnoce: Response) => {
                    return {token: respnoce.json().access_token,userEmail:respnoce.json().userEmail};
                }
            )
            //after subscribe method fired this method to store token into localStorage
            .do(
                tokenData => {
                    localStorage.setItem('token', tokenData.token);
                    this.eventForHeaderComponent.emit('fire!');
                    this.router.navigate(['/posts']);
                }
            );
    }

    logOut() {
        localStorage.removeItem("token");
        this.router.navigate(['/home']);
    }

    getToken() {
        return localStorage.getItem('token');
    }

    isAuthenticated() {
        let token = this.getToken();
        if (token) {
            return true;
        } else {
            return false;
        }
    }

    getUser() {
        const token = this.getToken();
        // Authorization  Bearer token
        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });
        return this.http.get('http://localhost:20377/api/users/getUser',options)
            .map(
                (response: Response) => {
                    // console.log(response.json()[0].userId);
                    this.currentUser.id = response.json()[0].userId;
                    this.currentUser.email = response.json()[0].email;
                    this.currentUser.role = response.json()[0].role;
                    return response;
                }
            );
    }

    isAdmin() {
        if (this.currentUser.role == 'admin') {
            return true;
        }
        return false;
    }


}
