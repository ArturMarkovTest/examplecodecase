import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthLoginComponent} from './auth-login/auth-login.component';
import {authRouting} from "./auth.routing";
import { ReactiveFormsModule } from '@angular/forms';
import { AuthRegistrationComponent } from './auth-registration/auth-registration.component';
import {AuthService} from "./auth.service";
import { ProtectedExampleComponent } from './protected-example/protected-example.component';
import {AuthGuard} from "./auth.guard";

@NgModule({
    imports: [
        CommonModule,
        authRouting,
        ReactiveFormsModule
    ],
    providers:[AuthService,AuthGuard],
    declarations: [AuthLoginComponent, AuthRegistrationComponent, ProtectedExampleComponent]
})
export class AuthModuleModule {
}
