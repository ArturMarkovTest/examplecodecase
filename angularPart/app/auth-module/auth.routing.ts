import {RouterModule, Routes} from "@angular/router";
import {AuthLoginComponent} from "./auth-login/auth-login.component";
import {AuthRegistrationComponent} from "./auth-registration/auth-registration.component";
import {ProtectedExampleComponent} from "./protected-example/protected-example.component";
import {AuthGuard} from "./auth.guard";


const AUTH_ROUTES: Routes = [
    {path: 'login', component: AuthLoginComponent},
    {path: 'registration', component: AuthRegistrationComponent },
    {path: 'protectedExample', component: ProtectedExampleComponent ,canActivate:[AuthGuard]}
];

export const authRouting = RouterModule.forChild(AUTH_ROUTES);