import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {PostsService} from "../posts.service";
import {Post} from "../abstract/post";
import {AuthService} from "../../auth-module/auth.service";
declare var swal: any;
import {MomentJsConverter} from "../Helpers/MomentJsConverter";

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

    constructor(private router: Router, private postsService: PostsService, private authService: AuthService) {
    }

    public posts: Array<Post>;

    ngOnInit() {
        this.postsService.getPosts().subscribe(
            posts => {
                //Order posts by date created_at
                this.posts = posts.sort((a,b)=> b.created_at.getTime() - a.created_at.getTime());
            }
        );
    }

    MomentJsConverter(date:Date){
        return MomentJsConverter.ConvertDate(date);
    }
    LimitText(text:string){
        if(text.length>150){
          return  text.substring(0,150)+ " .....";
        }
    }
    ViewPost(id){
        this.router.navigate(['/viewPost'], {queryParams: {'id':id}});
    }


    onUpdate(post: Post) {
        this.router.navigate(['/updatePost'], {queryParams: {'id': post.id}});
    }

    onCreatePosts() {
        this.router.navigate(['/createPost']);
    }

    public isPostOwner(user_id) {
        return this.authService.currentUser.id == user_id;
    }
    public isAdmin(){
        return this.authService.isAdmin();
    }
    public onDelete(idPost){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(()=> {
            this.postsService.deletePost(idPost).subscribe(
                ()=>{
                    let posts =  this.posts;
                    posts.forEach((post,index)=> {
                        if(post.id == idPost){
                            posts.splice(index,1);
                        }
                    });
                }
            );
        }).then(()=>{
            swal(
                'Deleted!',
                'Post has been deleted.',
                'success'
            );
        })

    }

}
