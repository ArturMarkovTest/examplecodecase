import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PostsComponent} from './posts/posts.component';
import {AuthGuard} from "../auth-module/auth.guard";
import {postsRouting} from "./posts.routing";
import { CreatePostComponent } from './create-post/create-post.component';
import {ReactiveFormsModule} from "@angular/forms";
import {PostsService} from "./posts.service";
import { UpdatePostComponent } from './update-post/update-post.component';
import { ViewPostComponent } from './view-post/view-post.component';

@NgModule({
    imports: [
        CommonModule,
        postsRouting,
        ReactiveFormsModule
    ],
    providers: [AuthGuard,PostsService],
    declarations: [PostsComponent, CreatePostComponent, UpdatePostComponent, ViewPostComponent]
})
export class PostsModuleModule {
}
