import * as moment from 'moment';

export class MomentJsConverter {


    public static ConvertDate(date:Date){
        return moment(date).format('DD/MM/YYYY | HH:mm:ss');
    }

}
