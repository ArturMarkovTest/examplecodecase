import {RouterModule, Routes} from "@angular/router";
import {PostsComponent} from "./posts/posts.component";
import {AuthGuard} from "../auth-module/auth.guard";
import {CreatePostComponent} from "./create-post/create-post.component";
import {UpdatePostComponent} from "./update-post/update-post.component";
import {ViewPostComponent} from "./view-post/view-post.component";


const POSTS_ROUTES: Routes = [
    {path: 'posts', component:PostsComponent ,canActivate:[AuthGuard]},
    {path: 'createPost', component:CreatePostComponent ,canActivate:[AuthGuard]},
    {path: 'updatePost', component:UpdatePostComponent ,canActivate:[AuthGuard]},
    {path: 'viewPost', component:ViewPostComponent ,canActivate:[AuthGuard]},
];

export const postsRouting = RouterModule.forChild(POSTS_ROUTES);