import {Injectable} from '@angular/core';
import {RequestOptions, Headers, Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {AuthService} from "../auth-module/auth.service";
import {Post} from "./abstract/post";
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {
    public serverUri: string = 'http://localhost:20377';

    constructor(private http: Http, private authService: AuthService) {
    }


    public createPost(title: string, body: string, fileList) {
        const token = this.authService.getToken();
        let formData: FormData = new FormData();
        let file: File = null;
        if (fileList != null && fileList.length > 0) {
            file = fileList[0];
            formData.append('File', file, file.name);
        }
        formData.append('Title', title);
        formData.append('Body', body);

        let headers = new Headers({ 'Authorization': `Bearer ${token}`});
        let options = new RequestOptions({ headers: headers });

        return this.http.post('http://localhost:20377/api/posts', formData,options);


    }

    public updatePost(id: string, title: string, body: string, fileList) {
        const token = this.authService.getToken();
        let formData: FormData = new FormData();
        let file: File = null;

        if (fileList != null && fileList.length > 0) {
            file = fileList[0];
            formData.append('File', file, file.name);
        }
        formData.append('Title', title);
        formData.append('Body', body);

        let headers = new Headers({ 'Authorization': `Bearer ${token}`});
        let options = new RequestOptions({ headers: headers });

        return this.http.put('http://localhost:20377/api/posts/'+id, formData,options);

    }

    public deletePostImg(id: string) {
        const token = this.authService.getToken();
        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });
        return this.http.delete('http://localhost:20377/api/posts/img/'+id,options).map(
            (response:Response)=>{
                return response;
            }
        );
    }

    public getPosts() {
        const token = this.authService.getToken();

        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });

        return this.http.get('http://localhost:20377/api/posts', options)
            .map(
                (response: Response) => {
                    let posts = response.json();
                    let newPosts: Array<Post> = [];
                    posts.forEach((postInResponse) => {
                        let customPathToImg: string = 'http://lorempixel.com/242/200/';
                        //length 12 is a default img path without uploaded photo
                        if (postInResponse.pathToFile.length>12 ) {
                            customPathToImg = this.serverUri + postInResponse.pathToFile;
                        }
                        var created_post  = new Date(postInResponse.created);
                        let post = new Post(postInResponse.id, postInResponse.userId, postInResponse.title,
                            postInResponse.body,
                            customPathToImg,
                            created_post
                         );
                        newPosts.push(post);
                    })
                    return newPosts;
                }
            );
    }

    public deletePost(idPost: string) {
        const token = this.authService.getToken();

        let headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Content-Type','application/json');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({ headers: headers });
        return this.http.delete('http://localhost:20377/api/posts/'+idPost,options);
    }

}
