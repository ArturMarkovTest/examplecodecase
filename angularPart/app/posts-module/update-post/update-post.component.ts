import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {PostsService} from "../posts.service";
import {Post} from "../abstract/post";
import {FormControl, FormGroup, Validators} from "@angular/forms";
declare var swal: any;
@Component({
    selector: 'app-update-post',
    templateUrl: './update-post.component.html',
    styleUrls: ['./update-post.component.css']
})
export class UpdatePostComponent implements OnDestroy {
    private subscription: Subscription;
    public post: Post;
    public updatePostForm: FormGroup;
    public showUploadImg: boolean = false;
    public file: FileList;

    constructor(private router: Router, private activetedRoute: ActivatedRoute, private postsService: PostsService) {
        this.subscription = activetedRoute.queryParams.subscribe(
            (queryParams: any) => {
                let postId = queryParams['id'];
                postsService.getPosts().subscribe(
                    (posts) => {
                        posts.filter((post: Post) => {
                            if (post.id == postId) {
                                this.post = post;
                                this.initForm();
                            }
                        })
                    }
                );
            }
        );
        this.updatePostForm = new FormGroup({
            'title': new FormControl(null, [<any>Validators.required]),
            'body': new FormControl(null, [<any>Validators.required]),
        });
    }

    public  onSubmit(data: any) {
        // let file = null;
        // if(!this.checkImgSource(this.post.pathToImg)){
        //     file = this.post.pathToImg;
        // }
        // file
        this.postsService.updatePost(this.post.id, data.title, data.body,this.file).subscribe(
            () => {
                swal(
                    'Success!',
                    'Post successfully updated!',
                    'success'
                );
                this.router.navigate(['/posts']);
            },
            error => console.log(error)
        )
    }

    public  initForm() {
        this.updatePostForm.controls['title'].setValue(this.post.title);
        this.updatePostForm.controls['body'].setValue(this.post.body);
    }

    public fileChange(event) {
        this.file = event.target.files;
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public checkImgSource(imgSrc: string): boolean {
        if (imgSrc != null) {
            return imgSrc.includes('lorempixel');
        }
        return false;
    }

    public onDeleteImg(imgSrc: string) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(() => {
                if (!this.checkImgSource(imgSrc)) {
                    //    we have original image
                    this.postsService.deletePostImg(this.post.id).subscribe(
                        result => {
                            console.log(result);
                        }
                    );
                }
                this.showUploadImg = true;
            }
        ).then(() => {
                swal(
                    'Deleted!',
                    'Image has been deleted.',
                    'success'
                );
            }
        )
    }


}
