import {Component } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {PostsService} from "../posts.service";
import {Router} from "@angular/router";
declare var swal:any;

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent   {
    public createPostForm: FormGroup;
    public file: FileList;

    constructor(private postsService: PostsService,private router: Router) {
        this.createPostForm = new FormGroup({
            'title': new FormControl(null, [<any>Validators.required]),
            'body': new FormControl(null, [<any>Validators.required]),
        });
    }

    public fileChange(event) {
        this.file = event.target.files;
    }

    public onSubmit(data: any) {
        this.postsService.createPost(data.title, data.body, this.file).subscribe(
            () => {
                swal(
                    'Good job!',
                    'You created post!',
                    'success'
                )
                this.router.navigate(['/posts']);
            },
            error =>{
                swal(
                    'Error',
                    error,
                    'error'
                );
            }
        )
    }


}
