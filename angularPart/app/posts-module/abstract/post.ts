export class Post {
    public id: string;
    public user_id: string;
    public title: string;
    public body: string;
    public pathToImg: string;
    public created_at: Date;

    constructor(id: string,user_id: string, title: string, body: string, pathToImg: string, created_at: Date) {
        this.id = id;
        this.user_id = user_id;
        this.title = title;
        this.body = body;
        this.pathToImg = pathToImg;
        this.created_at = created_at;
    }
}
