import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PostsService} from "../posts.service";
import {Subscription} from "rxjs";
import {Post} from "../abstract/post";
import {MomentJsConverter} from "../Helpers/MomentJsConverter";

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnDestroy {
  private subscription: Subscription;
  public post: Post;

  constructor(private router: Router, private activetedRoute: ActivatedRoute, private postsService: PostsService) {
    this.subscription = activetedRoute.queryParams.subscribe(
        (queryParams: any) => {
          let postId = queryParams['id'];
          postsService.getPosts().subscribe(
              (posts) => {
                posts.filter((post: Post) => {
                  if (post.id == postId) {
                    this.post = post;
                  }
                })
              }
          );
        }
    );
  }
  MomentJsConverter(date:Date){
    return MomentJsConverter.ConvertDate(date);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
