import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AuthModuleModule} from './auth-module/auth-module.module';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {DropdownDirective} from './header/dropdown.directive';
import {routing} from './app.routes';
import { HomeComponent } from './home/home.component';
import {AdminModuleModule} from "./admin-module/admin-module.module";
import {PostsModuleModule} from "./posts-module/posts-module.module";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        DropdownDirective,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AuthModuleModule,
        AdminModuleModule,
        PostsModuleModule,
        routing,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
