﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using MainApp.Helpers;
using MainApp.Models;
using MainApp.Repositories.Auth;
using MainApp.Models.Auth;
using MainApp.Repositories.Abstract.Auth;
using MainApp.ViewModel.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace MainApp.Controllers.Auth
{
    [Route("api/users")]
    public class AccountController : Controller
    {
        private readonly IUsersRepository _userRepository;

        public AccountController(IUsersRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("registration", Name = "userRegistration")]
        public IActionResult Registration([FromBody] AuthUser user)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return BadRequest(JsonConvert.SerializeObject(errorList));
            }
            else
            {
                dynamic result = _userRepository.CreateUser(user);
                if (result.StatusCode == 200)
                {
                    return Ok(result.Message);
                }
                return BadRequest(result.Message);
            }

        }

        [HttpPost("login", Name = "userLogin")]
        public IActionResult Login([FromBody] AuthUser user)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return BadRequest(JsonConvert.SerializeObject(errorList));
            }

            var identity = _userRepository.GetIdentity(user);
            if (identity == null)
            {
                return BadRequest("Invalid username or password.");
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                access_token = encodedJwt,
                userEmail = identity.Name,
            };
            return Ok(response);
        }
        [Authorize]
        [HttpGet("getUser", Name = "GetInfoAboutUser")]
        public IActionResult GetUser()
        {
            string userEmail = User.Identity.Name;
            var user = _userRepository.GetInfoAboutUser(userEmail);
            return Ok(user);
        }


    }
}
