﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using MainApp.Models.Auth;
using MainApp.Repositories.Abstract.Auth;
using MainApp.Repositories.Abstract.Post;
using MainApp.ViewModel.Posts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace MainApp.Controllers.Post
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [Authorize]
    public class PostsController : Controller
    {
        private readonly IPostsRepository _postsRepository;
        private readonly IUsersRepository _usersRepository;

        public PostsController(IPostsRepository postRepository, IUsersRepository userRepository)
        {
            _postsRepository = postRepository;
            _usersRepository = userRepository;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_postsRepository.GetAllPosts());
        }


        [HttpPost]
        public IActionResult CreatePost([FromForm] PostCreate post)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return BadRequest(JsonConvert.SerializeObject(errorList));
            }
            else
            {
                string userEmail = User.Identity.Name;
                return Ok(_postsRepository.CreatePost(post, userEmail));
            }
        }
        [HttpPut("{id}")]
        public IActionResult UpdatePost(int id,[FromForm] PostCreate post)
        {
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return BadRequest(JsonConvert.SerializeObject(errorList));
            }
            else
            {
                string userEmail = User.Identity.Name;
                return Ok(_postsRepository.UpdatePost(id,post, userEmail));
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            string userEmail = User.Identity.Name;
            User user = _usersRepository.GetUser(userEmail);
            if (_postsRepository.DeletePost(id, user))
            {
              return  Ok("Successfully deleted post");
            }
            return BadRequest("Post not deleted");

        }


        [HttpDelete("img/{id}")]
        public IActionResult DeletePostImg(int id)
        {
            string userEmail = User.Identity.Name;
            User user = _usersRepository.GetUser(userEmail);
            if (_postsRepository.DeletePostImg(id, user))
            {
                return Ok("Successfully deleted img");
            }
            return BadRequest("Img not deleted");

        }
    }
}
