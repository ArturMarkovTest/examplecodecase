﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MainApp.Repositories.Abstract.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace MainApp.Controllers.Admin
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [Authorize(Roles = "admin")]
    public class AdminController:Controller
    {
        private readonly IUsersRepository _userRepository;

        public AdminController(IUsersRepository userRepository)
        {
            _userRepository = userRepository;
        }
        [Route("getUsers")]
        [HttpGet]
        public IActionResult GetUsers()
        {
            return Ok(_userRepository.GetUsers());
        }
        [HttpDelete("user/{id}")]
        public IActionResult DeleteUser(int id)
        {
            if (_userRepository.DeleteUser(id))
            {
                return Ok(id);
            }
            return BadRequest("User was not deleted");
        }
    }
}
