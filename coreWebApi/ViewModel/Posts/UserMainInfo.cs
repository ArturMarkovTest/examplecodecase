﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MainApp.ViewModel.Posts
{
    public class UserMainInfo
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Role{ get; set; }
    }
}
