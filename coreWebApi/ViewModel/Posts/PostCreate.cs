﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace MainApp.ViewModel.Posts
{
    public class PostCreate
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }

        public IFormFile File { get; set; }        
    }
}
