﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MainApp.Models.Auth;
using MainApp.Models;
using MainApp.Repositories.Abstract.Post;
using MainApp.ViewModel.Posts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace MainApp.Repositories.Post
{
    public class PostsRepository:IPostsRepository
    {
        private readonly ApplicationContext _context;
        private readonly IHostingEnvironment _appEnvironment;
        private string _pathToFolder = "/Post_Files/";
        public PostsRepository(ApplicationContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        public Models.Posts.Post CreatePost(PostCreate post, string userEmail)
        {
            var user = _context.Users.FirstOrDefault(u => u.Email == userEmail);
            if (post.File != null)
            {
                CreatePostFile(post.File);
            }
            Models.Posts.Post createdPost = new Models.Posts.Post { Body = post.Body, Title = post.Title, PathToFile = _pathToFolder, UserId = user.Id, User = user,Created = DateTime.Now };
            _context.Posts.Add(createdPost);
            _context.SaveChanges();
            return createdPost;
        }
        public bool UpdatePost(int id ,PostCreate post, string userEmail)
        {
            var user = _context.Users.FirstOrDefault(u => u.Email == userEmail);
            Role role = _context.Roles.FirstOrDefault(r => r.Id == user.RoleId);
            var postToUpdate = _context.Posts.FirstOrDefault(p => p.Id == id && p.UserId == user.Id);
            if (postToUpdate != null)
            {
                postToUpdate.Title = post.Title;
                postToUpdate.Body= post.Body;
                if (post.File != null)
                {
                    CreatePostFile(post.File);
                    postToUpdate.PathToFile = _pathToFolder;
                }
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        private void CreatePostFile(IFormFile file)
        {
            _pathToFolder +=
                Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 8)
                             + file.FileName;
            using (var fileStream = new FileStream(_appEnvironment.WebRootPath + _pathToFolder, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
        }

        public List<Models.Posts.Post> GetAllPosts()
        {
            return _context.Posts.ToList();
        }

   

        public bool DeletePost(int id,User user)
        {
            Role role = _context.Roles.FirstOrDefault(r=>r.Id==user.RoleId);
            var  post = _context.Posts.FirstOrDefault(p => p.Id == id && p.UserId == user.Id ||role.Name=="admin");
            if (post != null)
            {
                _context.Posts.Remove(post);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeletePostImg(int id, User user)
        {
            Role role = _context.Roles.FirstOrDefault(r => r.Id == user.RoleId);
            var post = _context.Posts.FirstOrDefault(p => p.Id == id && p.UserId == user.Id || role.Name == "admin");
            if (post != null)
            {
                File.Delete(_appEnvironment.WebRootPath+post.PathToFile);
                //set default path to post img
                post.PathToFile = _pathToFolder;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

  
    }
}
