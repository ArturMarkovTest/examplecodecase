﻿using System.Collections.Generic;
using System.Security.Claims;
using MainApp.Models.Auth;
using MainApp.ViewModel.Auth;

namespace MainApp.Repositories.Abstract.Auth
{
    public interface IUsersRepository
    {
        object CreateUser(AuthUser user);
        ClaimsIdentity GetIdentity(AuthUser user);

        object GetInfoAboutUser(string userEmail);
        List<UserView> GetUsers();
        bool DeleteUser(int id);
        User GetUser(string email);
    }
}
