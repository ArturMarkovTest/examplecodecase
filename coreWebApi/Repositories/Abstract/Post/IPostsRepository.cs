﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MainApp.Models.Auth;
using MainApp.ViewModel.Posts;

namespace MainApp.Repositories.Abstract.Post
{
    public interface IPostsRepository
    {
        Models.Posts.Post CreatePost(PostCreate post,string  userEmail);
        List<Models.Posts.Post> GetAllPosts();
        bool DeletePost(int id, User user);
        bool DeletePostImg(int id, User user);
        bool UpdatePost(int id,PostCreate post, string userEmail);


    }
}
