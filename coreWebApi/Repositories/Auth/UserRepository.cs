﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MainApp.Helpers;
using MainApp.Models;
using MainApp.Models.Auth;
using MainApp.Repositories.Abstract.Auth;
using MainApp.ViewModel.Auth;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.IdentityModel.Logging;

namespace MainApp.Repositories.Auth
{
    public class UserRepository : IUsersRepository
    {
        private readonly ApplicationContext _context;

        public UserRepository(ApplicationContext context)
        {
            _context = context;
        }

        public object CreateUser(AuthUser user)
        {
            try
            {

                var userRole = _context.Roles.FirstOrDefault(r => r.Name == "user");
                _context.Users.Add(new User
                {
                    Email = user.Email,
                    Password = Password.Encrypt(user.Password),
                    Role = userRole,
                    RoleId = userRole.Id

                });
                _context.SaveChanges();
                return new { StatusCode = 200, Message = "Successfully created user!" };
            }

            catch (Exception ex)
            {
                LogHelper.LogExceptionMessage(ex);
                if (ex.InnerException != null)
                {
                    var sqlException = ex.InnerException as System.Data.SqlClient.SqlException;
                    if (sqlException.Number == 2601 || sqlException.Number == 2627)
                    {
                        return new { StatusCode = 400, Message = "This email already exist." };
                    }
                    return new { StatusCode = 400, Message = ex.InnerException.Message };
                }
                return new { StatusCode = 400, Message = ex.Message };
            }
        }

        public ClaimsIdentity GetIdentity(AuthUser authUser)
        {

            User user =
                _context.Users.FirstOrDefault(
                    u => u.Email == authUser.Email && u.Password == Password.Encrypt(authUser.Password));
            if (user != null)
            {
                int? idRole = user.RoleId;
                string nameRole = _context.Roles.FirstOrDefault(r => r.Id == idRole).Name;
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, nameRole)
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }

        public object GetInfoAboutUser(string userEmail)
        {
            return _context.Users.Where(user => user.Email == userEmail).Join(_context.Roles,
                user => user.RoleId,
                role => role.Id,
                (user, role) => new { userId = user.Id, email = user.Email, role = role.Name }
                ).Select(result => result);

        }

        public List<UserView> GetUsers()
        {
           return  _context.Users.Where(u => u.Role.Name != "admin")
                .Select(user => new UserView {Id = user.Id, Email = user.Email}).ToList();
        }

        public bool DeleteUser(int id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public User GetUser(string email)
        {
            return _context.Users.FirstOrDefault(u => u.Email == email);
        }
    }
}
