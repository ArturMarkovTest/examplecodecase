﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MainApp.Models.Auth;

namespace MainApp.Models.Posts
{
    public class Post
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        public string PathToFile { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }
        public DateTime Created { get; set; }
    }
}
