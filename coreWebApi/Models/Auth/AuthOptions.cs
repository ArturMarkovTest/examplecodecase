﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace MainApp.Models.Auth
{
    public class AuthOptions
    {
        public const string ISSUER = "MainAppAPI"; 
        public const string AUDIENCE = "http://localhost:20377/"; 
        const string KEY = "MarkovArtur1994!244650";  
        public const int LIFETIME = 30; 
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
